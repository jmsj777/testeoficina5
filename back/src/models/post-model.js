"use scrict"

const mongoose = require("mongoose")
const Schema = mongoose.Schema

const schema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  body: {
    type: String,
    required: true
  },
  comments: [
    {
      name: {
        type: String,
        required: true
      },
      email: {
        type: String,
        required: true
      },
      body: {
        type: String,
        required: true
      }
    }
  ]
})

module.exports = mongoose.model("Post", schema)
